enum InterActionTypes { like, love, care, haha, wow, sad, angry }

class Interaction {
  final int interactionCount;
  final InterActionTypes interActionType;

  Interaction({required this.interactionCount, required this.interActionType});

  @override
  String toString() {

    return "interactionCount= $interactionCount \n interactionType= $interActionType";
  }
}