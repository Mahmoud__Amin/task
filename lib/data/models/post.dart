part of "api_response.dart";
class Post {
  int? id;
  int? userId;
  int? parentId;
  String? modelType;
  int? modelId;
  String? content;
  int? index;
  int? status;
  int? type;
  String? createdAt;
  String? updatedAt;
  int? interactionsCount;
  InteractionsTypes? interactionsCountTypes;
  int? commentsCount;
  int? sharesCount;
  int? tagsCount;
  bool? sharingPost;
  bool? hasMedia;
  bool? saved;
  bool? taged;
  User? model;
  List<Media>? media;
  List<dynamic>? tags;
  dynamic parent;

  Post(
      {this.id,
        this.userId,
        this.parentId,
        this.modelType,
        this.modelId,
        this.content,
        this.index,
        this.status,
        this.type,
        this.createdAt,
        this.updatedAt,
        this.interactionsCount,
        this.interactionsCountTypes,
        this.commentsCount,
        this.sharesCount,
        this.tagsCount,
        this.sharingPost,
        this.hasMedia,
        this.saved,
        this.taged,
        this.model,
        this.media,
        this.tags,
        this.parent});

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    parentId = json['parent_id'];
    modelType = json['model_type'];
    modelId = json['model_id'];
    content = json['content'];
    index = json['index'];
    status = json['status'];
    type = json['type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    interactionsCount = json['interactions_count'];
    interactionsCountTypes = json['interactions_count_types'] != null
        ?  InteractionsTypes.fromJson(json['interactions_count_types'])
        : null;
    commentsCount = json['comments_count'];
    sharesCount = json['shares_count'];
    tagsCount = json['tags_count'];
    sharingPost = json['sharing_post'];
    hasMedia = json['has_media'];
    saved = json['saved'];
    taged = json['taged'];
    model = json['model'] != null ?  User.fromJson(json['model']) : null;
    if (json['media'] != null) {
      media = <Media>[];
      json['media'].forEach((v) {
        media!.add(Media.fromJson(v));
      });
    }
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((v) {
        tags!.add(v);
      });
    }
    parent = json['parent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content']=content;
    if (media != null) {
      data['media'] = media!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}