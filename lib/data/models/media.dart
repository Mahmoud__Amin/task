part of 'api_response.dart';
class Media {
  int? id;
  String? modelType;
  int? modelId;
  String? srcUrl;
  String? srcIcon;
  String? srcThum;
  String? collectionName;
  String? fullPath;
  String? mediaType;
  String? mimeType;
  int? size;
  int? width;
  int? height;
  String? createdAt;
  String? updatedAt;
  bool? saved;

  Media(
      {this.id,
        this.modelType,
        this.modelId,
        this.srcUrl,
        this.srcIcon,
        this.srcThum,
        this.collectionName,
        this.fullPath,
        this.mediaType,
        this.mimeType,
        this.size,
        this.width,
        this.height,
        this.createdAt,
        this.updatedAt,
        this.saved});

  Media.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    modelType = json['model_type'];
    modelId = json['model_id'];
    srcUrl = json['src_url'];
    srcIcon = json['src_icon'];
    srcThum = json['src_thum'];
    collectionName = json['collection_name'];
    fullPath = json['fullPath'];
    mediaType = json['media_type'];
    mimeType = json['mime_type'];
    size = json['size'];
    width = json['width'];
    height = json['height'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    saved = json['saved'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['src_url'] = srcUrl;
    data['media_type'] = mediaType;
    data['mime_type'] = mimeType;
    data['fullPath'] = fullPath;
    data['width'] = width;
    data['height'] = height;
    data['size'] = size;
    return data;
  }
}