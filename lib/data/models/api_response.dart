part 'data.dart';
part 'post.dart';
part 'interaction_types.dart';
part 'user.dart';
part 'media.dart';

class ApiResponse {
  int? status;
  int? errorCode;
  Data? data;
  String? message;

  ApiResponse({this.status, this.errorCode, this.data, this.message});

  ApiResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    errorCode = json['errorCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['status'] = status;
    data['errorCode'] = errorCode;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = message;
    return data;
  }
}

















