part of 'api_response.dart';
class User {
  int? id;
  int? userId;
  String? sn;
  String? firstName;
  String? middleName;
  String? lastName;
  int? gender;
  bool? isBlocked;
  dynamic blockedUntil;
  String? createdAt;
  String? updatedAt;
  String? lastSeen;
  String? name;
  int? isFriend;
  int? mutualFriendsCount;
  bool? screenBlock;
  bool? hasMediaProfile;
  bool? hasMediaCover;
  List<Media>? media;
  dynamic pronoun;
  String? username;
  String? email;
  String? phone;
  String? bio;
  int? followersCount;
  int? fansCount;
  bool? hasRole;
  bool? liked;
  bool? following;
  bool? starred;

  User(
      {this.id,
        this.userId,
        this.sn,
        this.firstName,
        this.middleName,
        this.lastName,
        this.gender,
        this.isBlocked,
        this.blockedUntil,
        this.createdAt,
        this.updatedAt,
        this.lastSeen,
        this.name,
        this.isFriend,
        this.mutualFriendsCount,
        this.screenBlock,
        this.hasMediaProfile,
        this.hasMediaCover,
        this.media,
        this.pronoun,
        this.username,
        this.email,
        this.phone,
        this.bio,
        this.followersCount,
        this.fansCount,
        this.hasRole,
        this.liked,
        this.following,
        this.starred});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    sn = json['sn'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    gender = json['gender'];
    isBlocked = json['isBlocked'];
    blockedUntil = json['blocked_until'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    lastSeen = json['last_seen'];
    name = json['name'];
    isFriend = json['is_friend'];
    mutualFriendsCount = json['mutualfriends_count'];
    screenBlock = json['screen_block'];
    hasMediaProfile = json['has_media_profile'];
    hasMediaCover = json['has_media_cover'];
    if (json['media'] != null) {
      media = <Media>[];
      json['media'].forEach((v) {
        media!.add( Media.fromJson(v));
      });
    }
    pronoun = json['pronoun'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    bio = json['bio'];
    followersCount = json['followers_count'];
    fansCount = json['fans_count'];
    hasRole = json['has_role'];
    liked = json['liked'];
    following = json['following'];
    starred = json['starred'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =new Map<String, dynamic>();
    data['id'] = id;
    data['user_id'] = userId;
    data['sn'] = sn;
    data['first_name'] = firstName;
    data['middle_name'] = middleName;
    data['last_name'] = lastName;
    data['gender'] = gender;
    data['isBlocked'] = isBlocked;
    data['blocked_until'] = blockedUntil;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['last_seen'] = lastSeen;
    data['name'] = name;
    data['is_friend'] = isFriend;
    data['mutualfriends_count'] = mutualFriendsCount;
    data['screen_block'] = screenBlock;
    data['has_media_profile'] = hasMediaProfile;
    data['has_media_cover'] = hasMediaCover;
    if (media != null) {
      data['media'] = media!.map((v) => v.toJson()).toList();
    }
    data['pronoun'] = pronoun;
    data['username'] = username;
    data['email'] = email;
    data['phone'] = phone;
    data['bio'] = bio;
    data['followers_count'] = followersCount;
    data['fans_count'] = fansCount;
    data['has_role'] = hasRole;
    data['liked'] = liked;
    data['following'] = following;
    data['starred'] = starred;
    return data;
  }
}