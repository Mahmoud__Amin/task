import 'dart:ui';

import 'package:flutter/material.dart';
class TaskColor {

  static Color color1 = Colors.black;

  static Color color2 = Colors.white;

  static Color color3 = Colors.grey;

  static Color color4 = Colors.blueAccent;

  static Color color5 = Colors.red;

  static Color color6 =Colors.green;
}

