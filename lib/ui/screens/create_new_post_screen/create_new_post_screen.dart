import 'package:Task/ui/screens/create_new_post_screen/bloc/create_new_post_events.dart';
import 'package:Task/ui/style/color/task_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../navigation/task_navigator.dart';
import '../../style/themes/task_theme.dart';
import '../../widgets/widget.dart';
import '../posts_screen/posts_screen.dart';
import 'bloc/create_new_post_bloc.dart';
import 'bloc/create_new_post_states.dart';

class CreateNewPostScreen extends StatelessWidget {
  const CreateNewPostScreen({super.key});

  static final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var bloc = context.read<CreateNewPostBloc>();
    return BlocConsumer<CreateNewPostBloc, CreateNewPostStates>(
      listener: (BuildContext context, Object? state) {
        if (state is CreateNewPostSuccessState) {
          navigateReplace(context, const PostsScreen());
        }
      },
      builder: (BuildContext context, state) {
        return Form(
          key: formKey,
          child: Scaffold(
              appBar: buildCreateNewPostScreenAppBar(bloc),
              body: ListView(
          padding: const EdgeInsets.all(10).w,
          children: [
            buildCreateNewPostScreenSizedBox(state),
            buildCreateNewPostUserInformation(bloc),
            buildCreateNewPostScreenTextFormField(bloc),
            TaskSizedBox(height: 10),
            TaskPostMediaGridView(pickedFiles: bloc.pickedFiles),
            TaskSizedBox(height: 70),
            buildCreateNewPostScreenOutlineButton(bloc,context),
          ],
        ),)
        ,
        );
      },
    );
  }
  Widget buildCreateNewPostScreenSizedBox(CreateNewPostStates state){
    if(state is CreateNewPostLoadingState || state is UploadFilesToFirebaseLoadingState){
      return Column(
        children: [
          const LinearProgressIndicator(),
          TaskSizedBox(height: 10,),
        ],
      );
    }
     return const SizedBox.shrink();
  }
  AppBar buildCreateNewPostScreenAppBar(CreateNewPostBloc bloc) {
    return AppBar(
      centerTitle: false,
      title: Text(
          "Create post",
          style: TaskTheme.lightTextTheme.headlineMedium
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.all(1).r,
          child: TextButton(
              onPressed: () {
                if (formKey.currentState!.validate()) {
                  bloc.add(UploadFilesIntoFirebaseEvent(
                      files: bloc.pickedFiles));
                }
              },
              child: Container(
                width: 70.w,
                height: 35.h,
                decoration: BoxDecoration(
                    color: TaskColor.color4,
                    borderRadius: BorderRadius.circular(5.r)),
                child: Center(
                  child: Text(
                    "POST",
                    style: TaskTheme.lightTextTheme.headlineMedium
                        ?.copyWith(
                        color: TaskColor.color2, fontSize: 18.sp),
                  ),
                ),
              )),
        ),
      ],
    );
  }

  Widget buildCreateNewPostScreenTextFormField(CreateNewPostBloc bloc){
    return TextFormField(
      maxLines: null,
      validator: (String? value) {
        if ((value == null || value.isEmpty) &&
            bloc.pickedFiles.isEmpty) {
          return "Content must not be empty !";
        }
        return null;
      },
      cursorColor: TaskColor.color4,
      controller: bloc.contentController,
      decoration: InputDecoration(
        hintText: (bloc.pickedFiles.isEmpty)
            ? "What's on your mind?"
            : "Say something about this...",
        hintStyle: TaskTheme.lightTextTheme.headlineMedium
            ?.copyWith(color: TaskColor.color3, fontSize: 25.sp),
        // Remove borders
      ),
    );
  }

  Widget buildCreateNewPostUserInformation(CreateNewPostBloc bloc){
    return Row(
      children: [
        TaskCircleImage(
            imageProvider: const AssetImage("assets/cr7_profile.jpg"),
            imageRadius: 30.r),
        TaskSizedBox(width: 10),
        Text("Cristiano Ronaldo",
            style: TaskTheme.lightTextTheme.headlineSmall
                ?.copyWith(fontWeight: FontWeight.bold)),
      ],
    );
  }

  Widget buildCreateNewPostScreenOutlineButton(CreateNewPostBloc bloc,BuildContext context){
    return OutlinedButton(
        onPressed: () {
          bloc.add(
              PickFilesFromGalleryButtonPressed(context: context));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.photo_library_outlined,
              color: TaskColor.color4,
            ),
            TaskSizedBox(
              width: 10,
            ),
            const Text("Add media")
          ],
        ));
  }


}
