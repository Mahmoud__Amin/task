

abstract class CreateNewPostStates{}

// create new posts states

class CreateNewPostInitialState extends CreateNewPostStates{}

class CreateNewPostLoadingState extends CreateNewPostStates{}

class CreateNewPostSuccessState extends CreateNewPostStates{
   CreateNewPostSuccessState();
}
class CreateNewPostErrorState extends CreateNewPostStates{
  final String error;
  CreateNewPostErrorState(this.error);
}

// upload file into firebase states

class UploadFilesToFirebaseLoadingState extends CreateNewPostStates{}

class UploadFilesToFirebaseSuccessState extends CreateNewPostStates{
  UploadFilesToFirebaseSuccessState();
}
class UploadFilesToFirebaseErrorState extends CreateNewPostStates{
  final String error;
  UploadFilesToFirebaseErrorState(this.error);
}

// pick files from gallery states

class PickFilesFromGalleryLoadingState extends CreateNewPostStates{}

class PickFilesFromGallerySuccessState extends CreateNewPostStates{
  PickFilesFromGallerySuccessState();
}
class PickFilesFromGalleryErrorState extends CreateNewPostStates{
  final String error;
  PickFilesFromGalleryErrorState(this.error);
}











