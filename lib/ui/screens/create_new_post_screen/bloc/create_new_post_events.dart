import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';

import '../../../../data/models/api_response.dart';


abstract class CreateNewPostEvents {}

class CreateNewPostButtonPressed extends CreateNewPostEvents {
  final Post post;
  CreateNewPostButtonPressed({required  this.post});
}

class UploadFilesIntoFirebaseEvent extends CreateNewPostEvents{
   final List<PlatformFile> files;

  UploadFilesIntoFirebaseEvent({required this.files});
}

class PickFilesFromGalleryButtonPressed extends CreateNewPostEvents {
  final BuildContext context;

  PickFilesFromGalleryButtonPressed({required this.context});
}









