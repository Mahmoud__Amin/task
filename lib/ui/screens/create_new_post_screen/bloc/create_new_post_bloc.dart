import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import '../../../../data/models/api_response.dart';
import '../../../../network/end_points.dart';
import '../../../../network/remote/dio_helper.dart';
import '../../../constants/media_suffiex.dart';
import 'create_new_post_events.dart';
import 'create_new_post_states.dart';

class CreateNewPostBloc extends Bloc<CreateNewPostEvents, CreateNewPostStates> {
  CreateNewPostBloc() : super(CreateNewPostInitialState()) {
    on<CreateNewPostButtonPressed>((event, emit) => createNewPost(event, emit));
    on<UploadFilesIntoFirebaseEvent>(
        (event, emit) => uploadFilesToFirebase(event, emit));
    on<PickFilesFromGalleryButtonPressed>(
        (event, emit) => pickFiles(event, emit));
  }

  List<PlatformFile> pickedFiles = [];
  List<Media> media=[];
  TextEditingController contentController =TextEditingController();

  Future<void> createNewPost(CreateNewPostButtonPressed event,
      Emitter<CreateNewPostStates> emit) async {
    emit(CreateNewPostLoadingState());
    try {
      var response = await DioHelper.postData(url: CREATEPOST, data: event.post.toJson());
      var data = response?.data;
      if (data['status'] == 201) {
        emit(CreateNewPostSuccessState());
      } else {
        emit(CreateNewPostErrorState(data['message']));
      }
    } catch (error) {
      emit(CreateNewPostErrorState(error.toString()));
    }
  }

  Future<void> pickFiles(PickFilesFromGalleryButtonPressed event,
    Emitter<CreateNewPostStates> emit) async {
    try {
      pickedFiles = [];
      emit(PickFilesFromGalleryLoadingState());
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: imageExtensionsList + videoExtensionsList,
        allowMultiple: true, // Enable multiple file selection
      );

      if (result != null) {
        pickedFiles.addAll(result.files);
        emit(PickFilesFromGallerySuccessState());
      } else {
        emit(PickFilesFromGallerySuccessState());
      }
    }
    catch(error){
      emit(PickFilesFromGalleryErrorState(error.toString()));
    }
  }

  Future<void> uploadFilesToFirebase(UploadFilesIntoFirebaseEvent event, Emitter<CreateNewPostStates> emit) async {
    emit(UploadFilesToFirebaseLoadingState());
    try {
      for (var file in event.files) {
        final Reference storageRef = FirebaseStorage.instance
            .ref()
            .child('uploads')
            .child('${DateTime.now().millisecondsSinceEpoch}_${file.name}');

        final UploadTask uploadTask = storageRef.putFile(File(file.path!));
        await uploadTask.whenComplete(() async {
          final url = await storageRef.getDownloadURL();
          media.add(Media(srcUrl: url,size: file.size,fullPath: file.path,mimeType: file.extension,height: 360,width: 360,mediaType:setMediaType(file.extension!) ));
        });
      }
    } catch (error) {
      emit(UploadFilesToFirebaseErrorState(error.toString()));
    }
    emit(UploadFilesToFirebaseSuccessState());
    add(CreateNewPostButtonPressed(post:Post(content: contentController.text,media: media)));
  }

String setMediaType(String fileExtension){
    if(imageExtensionsList.contains(fileExtension)){
      return "Image";
    }
    return "Video";
}
}
