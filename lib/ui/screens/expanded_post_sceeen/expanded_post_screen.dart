import 'package:Task/ui/style/color/task_colors.dart';
import 'package:Task/ui/widgets/choose_media_type.dart';
import 'package:Task/ui/widgets/task_post_interactions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../data/models/api_response.dart';
import '../../widgets/task_post_content_section.dart';
import '../../widgets/task_post_user_info_section.dart';
import '../../widgets/task_sized_box.dart';


class ExpandedPostScreen extends StatelessWidget {
   const ExpandedPostScreen({Key? key,required this.post}) : super(key: key);
   final Post post;
  @override
  Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(),
          body: ListView(
            primary: true,
            physics: const BouncingScrollPhysics(),
            children: [
            TaskPostUserInfoSection(post: post),
            if (post.content == null) TaskSizedBox(height: 15),
            if (post.content != null) TaskPostContentSection(post: post),
            if(post.interactionsCount!=0||post.commentsCount!=0||post.sharesCount!=0)TaskSizedBox(height: 15),
            if (post.interactionsCount != 0)TaskPostInteractions(interactionsTypes: post.interactionsCountTypes!,),
            buildExpandedPostScreenListView(),
            TaskSizedBox(height: 10,)
    ]));


}

Widget buildExpandedPostScreenListView(){
     if(post.media!.isNotEmpty) {
       return ListView.separated(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => ChooseMediaType(media: post.media![index]),
        separatorBuilder: (BuildContext context, int index) =>
            Container(
              color: TaskColor.color3,
              width: double.infinity.w,
              height: 5.h,
            ),
        itemCount: post.media!.length);
     }
     return const SizedBox.shrink();
   }


}
