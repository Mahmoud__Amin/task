import 'package:Task/ui/style/color/task_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:video_player/video_player.dart';


class VideoPlayerScreen extends StatefulWidget {
  const VideoPlayerScreen({super.key, required this.videoUrl});
  final String videoUrl;
  @override
  State<VideoPlayerScreen> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(
      Uri.parse(
       widget.videoUrl,
      ),
      videoPlayerOptions: VideoPlayerOptions(allowBackgroundPlayback: true),
    );


    _controller.addListener(() {
      if (_controller.value.position >= _controller.value.duration) {
        setState(() {
          _controller.pause();
        });
      }
    });
    _initializeVideoPlayerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 360.w,
        height: 360.h,
        child: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  toggleVideoState();
                });
              },
              child: AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: Stack(
                  children: [
                    VideoPlayer(_controller),
                    if (!_controller.value.isPlaying)
                    Align(
                          alignment: Alignment.center,
                          child: IconButton(
                              alignment: Alignment.center,
                              iconSize: 70.r,
                              onPressed: () {
                                setState(() {
                                  toggleVideoState();
                                });
                              },
                              icon: Icon(
                                Icons.play_circle_outline_outlined,
                                color: TaskColor.color2,
                              )))
                  ],
                ),
              ),
            );
          } else {
          return const Center(child: CircularProgressIndicator(),
            );
          }
        },
      ));

  }
  void playTheVideo(){
    _controller.play();
  }
  void stopTheVideo(){
    _controller.pause();
  }
  void toggleVideoState() {
    if (_controller.value.isPlaying) {
      stopTheVideo();
    } else {
      playTheVideo();
    }
  }

}
