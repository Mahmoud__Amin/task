import 'package:Task/ui/screens/posts_screen/bloc/posts_states.dart';
import 'package:Task/ui/style/color/task_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../widgets/widget.dart';
import 'bloc/posts_bloc.dart';

class PostsScreen extends StatelessWidget {
  const PostsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var bloc = context.read<PostsBloc>();
    return BlocConsumer<PostsBloc, PostsStates>(
      listener: (BuildContext context, Object? state) {},
      builder: (BuildContext context, state) {
        return Scaffold(
          backgroundColor: TaskColor.color3.withOpacity(0.8),
          appBar: AppBar(title: const Text('Facebook Posts')),
          body: buildPostsList(bloc),
        );
      },
    );
  }

  Widget buildPostsList(PostsBloc bloc) {
    if (bloc.posts.isNotEmpty) {
      return ListView.builder(
        padding: const EdgeInsets.only(top: 5).w,
        itemCount: bloc.posts.length,
        controller: bloc.scrollController,
        itemBuilder: (BuildContext context, int index) {
          final post = bloc.posts[index];
          return TaskPostCard(post: post);
        },
      );
    }
    return const Center(child: CircularProgressIndicator());
  }
}
