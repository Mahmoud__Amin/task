import 'package:Task/data/models/api_response.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import '../../../../network/end_points.dart';
import '../../../../network/remote/dio_helper.dart';
import 'posts_events.dart';
import 'posts_states.dart';

class PostsBloc extends Bloc<PostsEvents, PostsStates> {
  PostsBloc() : super(PostsInitialState()) {
    on<GetPostsForPostsScreen>((event, emit) => getPosts(event, emit));
  }
  final ScrollController scrollController = ScrollController();
  List<Post> posts = [];
  int currentPageIndex=1;
  int lastPageIndex=2;
  void initState() {
    scrollController.addListener(_scrollListener);
     add(GetPostsForPostsScreen(pageIndex: 1));
  }
  void _scrollListener() {
    if (scrollController.position.pixels >= scrollController.position.maxScrollExtent-500) {
      if(currentPageIndex<lastPageIndex){
        add(GetPostsForPostsScreen(pageIndex: currentPageIndex));
      }
    }
  }


  Future<void> getPosts(GetPostsForPostsScreen event,Emitter<PostsStates> emit) async {
    if(currentPageIndex==1)posts = [];
    if(state is GetPostsLoadingState)return;
    emit(GetPostsLoadingState());
    try {
      var response = await DioHelper.getData(url: "$GETPOSTS${event.pageIndex}",);
      var data = response?.data;
      currentPageIndex=data["data"]["current_page"]+1;
      lastPageIndex=data["data"]["last_page"];
      if (data['status'] == 200) {
        data["data"]["items"].forEach((item){
          posts.add(Post.fromJson(item));
        });
        emit(GetPostsSuccessState());
      } else {
        emit(GetPostsErrorState(data['message']));
      }
    } catch (error) {
      emit(GetPostsErrorState(error.toString()));
    }
  }
}
