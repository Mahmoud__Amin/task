import 'package:Task/ui/widgets/widget.dart';
import 'package:flutter/material.dart';

import '../../data/models/api_response.dart';

class TaskPostContentSection extends StatelessWidget {
  final Post post;
  const TaskPostContentSection({super.key, required this.post});
  @override
  Widget build(BuildContext context) {
    return TaskReadMoreText(
      text:post.content!,
    );
  }
}