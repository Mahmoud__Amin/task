import 'package:Task/ui/style/color/task_colors.dart';
import 'package:Task/ui/style/themes/task_theme.dart';
import 'package:flutter/material.dart';

class TaskPostBottom extends StatelessWidget {
  const TaskPostBottom({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonBar(
      alignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        TextButton.icon(
          onPressed: () {},
          icon: Icon(
            Icons.thumb_up_outlined,
            color: TaskColor.color3,
          ),
          label: Text(
            'Like',
            style: TaskTheme.lightTextTheme.bodyMedium
                ?.copyWith(color: TaskColor.color3),
          ),
        ),
        TextButton.icon(
          onPressed: () {},
          icon: Icon(
            Icons.comment_outlined,
            color: TaskColor.color3,
          ),
          label: Text(
            'Comment',
            style: TaskTheme.lightTextTheme.bodyMedium
                ?.copyWith(color: TaskColor.color3),
          ),
        ),
        TextButton.icon(
          onPressed: () {},
          icon: Icon(
            Icons.share,
            color: TaskColor.color3,
          ),
          label: Text(
            'Share',
            style: TaskTheme.lightTextTheme.bodyMedium
                ?.copyWith(color: TaskColor.color3),
          ),
        ),
      ],
    );
  }
}
