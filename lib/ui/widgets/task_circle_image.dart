import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TaskCircleImage extends StatelessWidget {
  const TaskCircleImage({
    super.key,
    this.imageProvider,
    this.imageRadius = 20,

  });

  final double imageRadius;
  final ImageProvider? imageProvider;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.white,
      radius: imageRadius.r,
      child: CircleAvatar(
        radius: imageRadius-5.r,
        backgroundImage: imageProvider,
      ),
    );
  }
}
