
import 'dart:io';

import 'package:Task/ui/screens/video_screen/video_player_screen.dart';
import 'package:Task/ui/widgets/choose_media_type.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../data/models/api_response.dart';
import '../constants/media_suffiex.dart';
import '../style/color/task_colors.dart';
import '../style/themes/task_theme.dart';

class TaskPostMediaGridView extends StatelessWidget {
  TaskPostMediaGridView({Key? key, this.media, this.pickedFiles})
      : super(key: key);
  final List<Media>? media;
  List<PlatformFile>? pickedFiles;

  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
            width: 360.w,
            height: 360.h,
            child: buildMediaGridView(List.generate(
                (pickedFiles == null) ? media!.length : pickedFiles!.length,
                (index) => (pickedFiles == null)
                    ? ChooseMediaType(media: media![index])
                    : chooseFileType(pickedFiles![index])))));
  }

  Widget buildMediaGridView(List<Widget> items) {
    if (items.isEmpty) {
      return const SizedBox.shrink();
    }

    final int itemCount = items.length;
    final int remainingCount = itemCount - 4;

    switch (itemCount) {
      case 1:
        return buildGridForOneItem(items);
      case 2:
        return buildGridForTwoItems(items);
      case 3:
        return buildGridForThreeItems(items);
      case 4:
        return buildGridForFourItems(items);
      case 5:
        return buildGridForFiveItems(items);
      default:
        return buildGridForMoreThanFiveItems(items, remainingCount);
    }
  }

  Widget buildGridForOneItem(List<Widget> items) {
    return StaggeredGrid.count(
      crossAxisCount: 4,
      mainAxisSpacing: 3,
      crossAxisSpacing: 3,
      children: [
        StaggeredGridTile.count(
          crossAxisCellCount: 4,
          mainAxisCellCount: 4,
          child: items[0],
        ),
      ],
    );
  }

  Widget buildGridForTwoItems(List<Widget> items) {
    return StaggeredGrid.count(
      crossAxisCount: 4,
      mainAxisSpacing: 3,
      crossAxisSpacing: 3,
      children: List.generate(items.length, (index) {
        return StaggeredGridTile.count(
          crossAxisCellCount: 4,
          mainAxisCellCount: 2,
          child: items[index],
        );
      }),
    );
  }

  Widget buildGridForThreeItems(List<Widget> items) {
    return StaggeredGrid.count(
        crossAxisCount: 4,
        mainAxisSpacing: 3,
        crossAxisSpacing: 3,
        children: List.generate(items.length, (index) {
          return StaggeredGridTile.count(
            crossAxisCellCount: (index == 0) ? 4 : 2,
            mainAxisCellCount: 2,
            child: items[index],
          );
        }));
  }

  Widget buildGridForFourItems(List<Widget> items) {
    return StaggeredGrid.count(
        crossAxisCount: 4,
        mainAxisSpacing: 3,
        crossAxisSpacing: 3,
        children: List.generate(items.length, (index) {
          return StaggeredGridTile.count(
            crossAxisCellCount: 2,
            mainAxisCellCount: 2,
            child: items[index],
          );
        }));
  }

  Widget buildGridForFiveItems(List<Widget> items) {
    return StaggeredGrid.count(
        crossAxisCount: 6,
        mainAxisSpacing: 3,
        crossAxisSpacing: 3,
        children: List.generate(items.length, (index) {
          return StaggeredGridTile.count(
            crossAxisCellCount: (index < 2) ? 3 : 2,
            mainAxisCellCount: (index < 2) ? 3.5 : 2.5,
            child: items[index],
          );
        }));
  }

  Widget buildGridForMoreThanFiveItems(List<Widget> items, int remainingCount) {
    final List<StaggeredGridTile> gridTiles = [];

    for (int i = 0; i < 4; i++) {
      gridTiles.add(StaggeredGridTile.count(
        crossAxisCellCount: (i < 2) ? 3 : 2,
        mainAxisCellCount: (i < 2) ? 3 : 2,
        child: items[i],
      ));
    }

    gridTiles.add(StaggeredGridTile.count(
      crossAxisCellCount: 2,
      mainAxisCellCount: 2,
      child: Stack(
        children: [
          items[4],
          Container(
            decoration: BoxDecoration(
              color: TaskColor.color2.withOpacity(0.4),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Text(
              '+$remainingCount',
              style: TaskTheme.lightTextTheme.headlineLarge
                  ?.copyWith(color: TaskColor.color2),
            ),
          ),
        ],
      ),
    ));

    return StaggeredGrid.count(
      crossAxisCount: 6,
      mainAxisSpacing: 3,
      crossAxisSpacing: 3,
      children: gridTiles,
    );
  }

  Widget chooseFileType(PlatformFile file) {
    if (imageExtensionsList.contains(file.extension)) {
      return Image.file(
        File(file.path!),
        width: 360.w,
        height: 360.h,
        fit: BoxFit.cover,
      );
    } else {
      return VideoPlayerScreen(videoUrl: file.path!);
    }
  }
}
