import 'package:Task/ui/navigation/task_navigator.dart';
import 'package:Task/ui/screens/expanded_post_sceeen/expanded_post_screen.dart';
import 'package:Task/ui/widgets/task_post_interactions.dart';
import 'package:Task/ui/widgets/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../data/models/api_response.dart';


class TaskPostCard extends StatelessWidget {
  final Post post;

  const TaskPostCard({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        navigateTo(context,ExpandedPostScreen(post: post));
      },
      child: Card(
          margin: const EdgeInsets.symmetric(vertical: 5).w,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TaskPostUserInfoSection(post: post),
                if (post.content == null) TaskSizedBox(height: 15),
                if (post.content != null) TaskPostContentSection(post: post),
                if (post.media!.isNotEmpty)TaskPostMediaGridView(media: post.media!),
                if(post.interactionsCount!=0||post.commentsCount!=0||post.sharesCount!=0)TaskSizedBox(height: 15),
                if (post.interactionsCount != 0)TaskPostInteractions(interactionsTypes: post.interactionsCountTypes!,),
                const TaskPostBottom()
              ])),
    );
  }


}
