import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../../data/models/api_response.dart';
import '../../data/models/interaction.dart';

class TaskPostInteractions extends StatelessWidget {
  TaskPostInteractions({Key? key, required this.interactionsTypes})
      : super(key: key);
  final InteractionsTypes interactionsTypes;
  var interactionsRow = Row(children: []);

  @override
  Widget build(BuildContext context) {
    addInteractionsIntoRow();
    return Padding(
      padding: const EdgeInsets.only(left: 8).w,
      child: interactionsRow,
    );
  }

  void addInteractionsIntoRow() {
    List<Interaction> interactions = [
      Interaction(
        interactionCount: interactionsTypes.like!,
        interActionType: InterActionTypes.like,
      ),
      Interaction(
          interactionCount: interactionsTypes.love!,
          interActionType: InterActionTypes.love),
      Interaction(
          interactionCount: interactionsTypes.haha!,
          interActionType: InterActionTypes.haha),
      Interaction(
          interactionCount: interactionsTypes.care!,
          interActionType: InterActionTypes.care),
      Interaction(
          interactionCount: interactionsTypes.sad!,
          interActionType: InterActionTypes.sad),
      Interaction(
          interactionCount: interactionsTypes.angry!,
          interActionType: InterActionTypes.angry),
      Interaction(
          interactionCount: interactionsTypes.wow!,
          interActionType: InterActionTypes.wow)
    ];
    // sort the interaction by the most common action
    interactions
        .sort((a, b) => (b.interactionCount.compareTo(a.interactionCount)));

    int counter = 0;
    for (int i = 0; i < interactions.length; ++i) {
      if (interactions[i].interactionCount > 0) {
        interactionsRow.children.add(
          SvgPicture.asset(
            "assets/${interactions[i].interActionType}.svg",
            height: 15.h,
            width: 15.w,
          ),
        );
        counter++;
        if (counter >= 3) break;
      }
    }
  }
}
