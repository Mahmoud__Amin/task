

import 'package:Task/data/models/api_response.dart';
import 'package:Task/ui/widgets/widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../utilities/task_date_utility.dart';
import '../style/color/task_colors.dart';
import '../style/themes/task_theme.dart';

class TaskPostUserInfoSection extends StatelessWidget {
  final Post post;

  const TaskPostUserInfoSection({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TaskCircleImage(imageProvider: chooseProfileImage(post).image, imageRadius: 30.r),
        TaskSizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              post.model!.name!,
              style: TaskTheme.lightTextTheme.headlineSmall
                  ?.copyWith(fontWeight: FontWeight.bold),
            ),
            Text(
              TaskDateUtility.formatPostDate(DateTime.parse(post.createdAt!)),
              style: TaskTheme.lightTextTheme.bodySmall
                  ?.copyWith(color: TaskColor.color3),
            ),
          ],
        ),
        const Spacer(),
        IconButton(onPressed: () {}, icon: const Icon(Icons.more_horiz_outlined))
      ],
    );
  }
  Image chooseProfileImage(Post post) {
    ImageProvider imageProvider = (post.model?.gender == 0)
        ? const AssetImage("assets/default_male.gif")
        : const AssetImage("assets/default_female.gif");
    if (post.model!.media!.isNotEmpty) {
      return Image(
        image: CachedNetworkImageProvider(post.model!.media![0].srcUrl!),
        fit: BoxFit.cover,
        loadingBuilder: (context, child, loadingProgress) {
          if (loadingProgress == null) return child;
          return Center(
            child: CircularProgressIndicator(
              value: loadingProgress.expectedTotalBytes != null
                  ? loadingProgress.cumulativeBytesLoaded /
                  loadingProgress.expectedTotalBytes!
                  : null,
            ),
          );
        },
        errorBuilder: (context, error, stackTrace) {
          return Image(
            image: imageProvider,
            fit: BoxFit.cover,
          );
        },
      );
    }
    return Image(
      image: imageProvider,
      fit: BoxFit.cover,
    );
  }

}