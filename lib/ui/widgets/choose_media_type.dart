
import 'package:Task/ui/widgets/task_sized_box.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../data/models/api_response.dart';
import '../screens/video_screen/video_player_screen.dart';
import '../style/color/task_colors.dart';
import '../style/themes/task_theme.dart';
class ChooseMediaType extends StatelessWidget {
  const ChooseMediaType({Key? key,required this.media}) : super(key: key);
  final Media media;
  @override
  Widget build(BuildContext context) {
    if (media.mediaType == "Image") {
      return CachedNetworkImage(
        width: 360.w,
        height: 360.h,
        imageUrl: media.srcUrl!,
        placeholder: (context, url) =>
        const Center(child: CircularProgressIndicator()),
        errorWidget: (context, url, error) =>
            Center(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.error, color: TaskColor.color5, size: 40.r,),
                TaskSizedBox(height: 10,),
                Text("Failed to download this image", style: TaskTheme.lightTextTheme.bodySmall,)
              ],
            )),
        fit: BoxFit.cover,
      );
    } else {
      return VideoPlayerScreen(videoUrl: media.srcUrl!);
    }
  }
}

