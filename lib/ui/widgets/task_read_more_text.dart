import 'package:Task/ui/style/themes/task_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:readmore/readmore.dart';

import '../style/color/task_colors.dart';

class TaskReadMoreText extends StatelessWidget {
  TaskReadMoreText({
    this.text = "",
    Key? key,
  }) : super(key: key);
  String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10).w,
      child: ReadMoreText(
        text,
        trimLines: 4,
        textAlign: TextAlign.left,
        textScaleFactor: 1.r,
        style: TaskTheme.lightTextTheme.bodySmall?.copyWith(fontSize: 14.5.sp,fontWeight: FontWeight.w500),
        colorClickableText: TaskColor.color3,
        trimMode: TrimMode.Line,
        trimCollapsedText: 'See more',
        trimExpandedText: ' See less',
      ),
    );
  }
}
