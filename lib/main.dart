import 'package:Task/ui/screens/create_new_post_screen/bloc/create_new_post_bloc.dart';
import 'package:Task/ui/screens/create_new_post_screen/create_new_post_screen.dart';
import 'package:Task/ui/screens/posts_screen/bloc/posts_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Task/network/remote/dio_helper.dart';
import 'package:Task/ui/style/themes/task_theme.dart';
import 'package:Task/utilities/task_bloc_observer.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  DioHelper.init();
  Bloc.observer = TaskBlocObserver();
  runApp(const Task());
}

class Task extends StatelessWidget {
  const Task({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => PostsBloc()..initState()),
        BlocProvider(create: (context) => CreateNewPostBloc()),
      ],
      child: ScreenUtilInit(
          designSize: const Size(360.0, 772.0),
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (context, widget) {
            ScreenUtil.init(context);
            return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0.sp),
                child: MaterialApp(
                  title: 'Task',
                  debugShowCheckedModeBanner: false,
                  theme: TaskTheme.light(),
                  home:  const CreateNewPostScreen(),
                ));
          }),
    );
  }
}
