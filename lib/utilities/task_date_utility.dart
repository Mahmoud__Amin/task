import 'package:flutter/material.dart';
class TaskDateUtility {

  static String  formatPostDate(DateTime dateTime) {
    final now = DateTime.now().toUtc();
    final difference = now.difference(dateTime.toUtc());

    if (difference.inSeconds < 60) {
      return '${difference.inSeconds} seconds ago';
    } else if (difference.inMinutes < 60) {
      return '${difference.inMinutes} minutes ago';
    } else if (difference.inHours < 24) {
      return '${difference.inHours} hours ago';
    } else if (difference.inDays < 30) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays < 365) {
      final months = (now.year - dateTime.year) * 12 + now.month - dateTime.month;
      return '$months months ago';
    } else {
      return '${now.year - dateTime.year} years ago';
    }
  }

  static String formatTime(DateTime dateTime) {
    int hour = dateTime.hour;
    int minute = dateTime.minute;
    String period = (hour < 12) ? "AM" : "PM"; // Determine AM or PM

    // Convert to 12-hour format
    if (hour > 12) {
      hour -= 12; // Subtract 12 for PM hours
    } else if (hour == 0) {
      hour = 12; // Handle midnight (12 AM)
    }

    // Format minutes with leading zero
    String formattedMinute = (minute < 10) ? "0$minute" : minute.toString();

    // Combine the formatted components
    String finalTime = "$hour:$formattedMinute $period";

    return finalTime;
  }

}

